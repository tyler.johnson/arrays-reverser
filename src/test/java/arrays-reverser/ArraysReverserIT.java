import org.junit.*;
import static org.junit.Assert.*;
import javax.ws.rs.client.*;
import java.util.*;
import javax.ws.rs.core.GenericType;

public class ArraysReverserIT {
  private static String ENDPOINT = "http://localhost:8080/reverse-arrays";

  @Test
  public void simpleSingle() {
    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(ENDPOINT);
    String response = webTarget.request().post(Entity.entity("[[5]]", "application/json"), String.class);

    System.out.println(response);
    assertEquals(response, "[[5]]");
  }

  @Test
  public void empty() {
    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(ENDPOINT);
    String response = webTarget.request().post(Entity.entity("[[]]", "application/json"), String.class);

    System.out.println(response);
    assertEquals(response, "[[]]");
  }

  @Test
  public void complex() {
    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(ENDPOINT);
    String response = webTarget.request().post(Entity.entity("[[5,2],[0,1,2],[],[9]]", "application/json"), String.class);

    System.out.println(response);
    assertEquals(response, "[[2,5],[2,1,0],[],[9]]");
  }
}
