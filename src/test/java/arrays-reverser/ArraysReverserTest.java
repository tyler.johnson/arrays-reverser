package arrays_reverser;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

public class ArraysReverserTest {
  @Test
  public void emptyArray() {
    List<List<Integer>> sequences = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList())));
    List<List<Integer>> reversed = ArraysReverser.reverse(sequences);

    for (List<Integer> seq : reversed) {
      assertEquals(seq.size(), 0);
    }
  }

  @Test
  public void oneSequenceOneValue() {
    List<List<Integer>> sequences = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(9))));
    List<List<Integer>> reversed = ArraysReverser.reverse(sequences);

    for (List<Integer> seq : reversed) {
      assertEquals(seq.size(), 1);
      assertEquals((int) seq.get(0), 9);
    }
  }

  @Test
  public void oneSequenceTwoValues() {
    List<List<Integer>> sequences = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(9, 8))));
    List<List<Integer>> reversed = ArraysReverser.reverse(sequences);

    for (List<Integer> seq : reversed) {
      assertEquals(seq.size(), 2);
      assertEquals((int) seq.get(0), 8);
      assertEquals((int) seq.get(1), 9);
    }
  }

  @Test
  public void twoSequencesTwoValues() {
    Vector<List<Integer>> sequences = new Vector<>();
    sequences.add(new ArrayList<>(Arrays.asList(9, 8)));
    sequences.add(new ArrayList<>(Arrays.asList(6, 7)));

    List<List<Integer>> reversed = ArraysReverser.reverse(sequences);

    assertEquals((int) reversed.get(0).get(0), 8);
    assertEquals((int) reversed.get(0).get(1), 9);
    assertEquals((int) reversed.get(1).get(0), 7);
    assertEquals((int) reversed.get(1).get(1), 6);
  }

  @Test
  public void twoSequencesOneEmpty() {
    Vector<List<Integer>> sequences = new Vector<>();
    sequences.add(new ArrayList<>(Arrays.asList(9, 8)));
    sequences.add(new ArrayList<>(Arrays.asList()));

    List<List<Integer>> reversed = ArraysReverser.reverse(sequences);

    assertEquals((int) reversed.get(0).get(0), 8);
    assertEquals((int) reversed.get(0).get(1), 9);
    assertEquals((int) reversed.get(1).size(), 0);
  }
}
